from django.contrib import admin
from artistas.models import Artista

# Register your models here.
class ArtistaAdmin(admin.ModelAdmin):
    pass
admin.site.register(Artista, ArtistaAdmin)