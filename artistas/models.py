from django.db import models

# Create your models here.

class Artista(models.Model):
    nombre = models.CharField(max_length = 50, blank = True)
    apellido = models.CharField(max_length = 50, blank = True)
    nombre_artistico = models.CharField(max_length = 50)
    fecha_nacimiento = models.DateField(null = True)

    def __str__(self):
        return '{0}'.format(self.nombre_artistico)